<?php
/*
Plugin Name: Reader for Wordpress Visual Form Bulder
Plugin URI: www.mssa.fi
Description: This plugin is developed for MSSA.fi. Its purpose is to read MySQL data generated by the Visual Form Builder to display it via shortcode [vfbr id="id" selectbox="Name of the select box to group items" textbox="Numbers of text boxes"].
Version: 1.1.2
*/

// --- FORMING AND DISPLAYING TABLE ---
// Generating tables to display on the webpage
//      $tableTitle is the title of the table
//      $dbArray is the array, which contains selected database information
//      $lines is the amount of lines to be generated
function generateTables($tableTitle, $dbArray, $lines) {

    // Collects HTML data
    $html = "";

    // Sinle/plural form for $unit
    sizeOf($dbArray) == 1 ? $unit = 'vene' : $unit = 'venettä';

    // Generating table
    $html .= "<table> <h3>$tableTitle: ".sizeOf($dbArray)." ".$unit."</h3>";

    for ($i = 0; $i < sizeOf($dbArray); $i++) {

        // Generating rows
        $html .= "<tr>";

        // Generating cells
        for ($j = 0; $j < $lines; $j++ ) {
            $html .= "<td style='text-transform: capitalize; width: ".(100/$lines)."%;'>".$dbArray[$i][$j]."</td>";
        }

        $html .= "</tr>";
    }

    $html .= "</table>";

    return $html;
}

// Function to process vfbr-tagged shortcodes
function process_form( $atts ) {

    // --- INITIALIZATIONS ---

    // Fields in shortcode
    $formId = "{$atts['id']}";
    // Converting formId to int
    $formId = (int)$formId;
    // Textboxes which should appear in the table in user-specified order
    $textboxes = "{$atts['textboxes']}";
    // Whether amount of participants is shown or hidden
    $showAmount = "{$atts['show_amount']}";

    // Grouping is based on either checkbox or selectbox value
    // If both exist, checkbox is preferred
    if (array_key_exists('radioset', $atts)) {
        // Checkbox, which value is utilized for groupping
        $grouppingBox = "{$atts['radioset']}";
        $fieldType = "radio";
    } else {
        // Selectbox to be utilized for groupping
        $grouppingBox = "{$atts['selectbox']}";
        $fieldType = "select";
    }

    // --- GETTING INFORMATION FROM THE DATABASE ---

    // Visual Form Builder's table, which stores entries
    $entriesTable = "wp_visual_form_builder_entries";
    // Visual Form Builder's table, which stores the structures of the forms
    $formFieldsTable = "wp_visual_form_builder_fields";

    // Connecting to MySQL
    $connect = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

    // Selecting database
    mysqli_select_db($connect, DB_NAME);
    // Choosing utf8 charset, otherwise data with symbols ö and ä will not be correctly processed
    mysqli_set_charset($connect, "utf8");

    // Getting the contents of user's selectbox
    $formFieldsQuery = mysqli_query($connect, "SELECT field_options, field_sequence FROM $formFieldsTable
        WHERE field_type = '$fieldType' AND form_id LIKE $formId AND field_name LIKE '$grouppingBox'");

    // Getting the contents of the form
    $entriesQuery = mysqli_query($connect, "SELECT form_id, data FROM $entriesTable WHERE form_id LIKE $formId");

    // Closing connection
    mysqli_close($connect);

    // --- DATA PROCESSING ---

    // Converting text boxes into array for further processing
    $textboxesArray = explode(',', $textboxes);

    $results = array();

    // Getting items from selectbox. They will form the groups
    while ($res = mysqli_fetch_assoc($formFieldsQuery)) {
        $results[] = $res;
    }

    foreach ($results as $formFieldsQuery) {
        // Getting group data from user-chosen selectbox
        // (unserializing Visual Form Builder's data as it is stored in a storable representation)
        $grouppingBoxItems = unserialize($formFieldsQuery['field_options']);
        // Getting id of user-chosen selectbox
        $grouppingBoxIdInForm = (int)$formFieldsQuery['field_sequence'];
    }

    // Getting and grouping submitted contents of the form
    $results = array();

    while ($res = mysqli_fetch_assoc($entriesQuery)) {
        $results[] = $res;
    }

    // Creating an array, which will contain items from selec box
    for ($i = 0; $i < sizeof($grouppingBoxItems); $i++) {
        $groups[$i] = array();
    }

    // This array is used to collect processed entries in order to remove duplicates
    $processedEntries = array();

    // Amount of non-duplicated items in resulting tables
    $amountOfItems = 0;

    foreach ($results as $entriesQuery) {

        // Filtering duplicates to ensure that user have not submitted the same form twice
        if ( ! in_array($entriesQuery, $processedEntries) ) {

            // Unserializing Visual Form Builder's data as it is stored in a storable representation
            $data = unserialize($entriesQuery['data']);

            for ($i = 0; $i < sizeof($grouppingBoxItems); $i++) {

                $tableContents = array();

                if ($data[$grouppingBoxIdInForm]['value'] == $grouppingBoxItems[$i]) {

                    // Forming the data array ($tableContents), which contains selected fields for each form result
                    for ($j = 0; $j < sizeOf($textboxesArray); $j++) {
                        array_push($tableContents, $data[(int)$textboxesArray[$j]]['value']);
                    }

                    // $groups array is storing all the collected data to display in the output table
                    array_push($groups[$i], $tableContents);

                    $amountOfItems++;

                }
            }

            // This is needed to get rid of possible duplicates on next cycle
            array_push($processedEntries, $entriesQuery);
        }
    }

    // Displaying the amounts before the tables
    if ($showAmount == "true") {
        echo "<h3><i> Osallistujamäärä tällä hetkellä on ".$amountOfItems."</i></h3>";
    }

    // Generating table for each group
    for ($i = 0; $i < sizeof($grouppingBoxItems); $i++) {
        echo generateTables($grouppingBoxItems[$i], $groups[$i], sizeOf($textboxesArray));
    }
}

// Calling Wordpress' function to process shortcode with vbfr-tag
add_shortcode( 'vfbr', 'process_form' );

?>