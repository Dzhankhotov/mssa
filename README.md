# READER FOR VISUAL FORM BUILDER

## General information
This [Wordpress](http://www.wordpress.com) plugin is developed for [mssa.fi](http://www.mssa.fi). It reads [Visual Form Bulder's](https://srd.wordpress.org/plugins/visual-form-builder/) data stored in MySQL database and displays it in a post.

Plugin can be accessed via a shortcode, for example:

`[vfbr id=4 checkbox="Valitse veneen luokka" textboxes="1, 4, 5" show_amount="true"]`


## Shortcode items

* **`vfbr`** refers to the plugin itself (i.e. **v**isual **f**orm **b**uilder **r**eader).

* **`id`** is the Visual Form Builder's form id.

* **`radioset`** is the name of the radio set used for grouping. Has advantage over `selectbox`, if used together.

* **`selectbox`** is the name of the select box used for grouping.

* **`textboxes`** are the numbers of the text inputs, which results are to be included in the tables as columns.

* **`show_amount`** shows total amount of items in table if `true` (note that code considers **true** as a *string*, not *boolean*).

## Installation

* Copy plugin-containing folder into `/wp-content/plugins/`

* Activate plugin via `Wordpress' dashboard -> Plugins -> Inactive`
