<?php

// PHP playground: http://sandbox.onlinephpfunctions.com/

// Initial serialized array
$serialized = 'a:12:{i:0;a:7:{s:2:"id";s:2:"25";s:4:"slug";s:13:"mssa-8-7-2017";s:4:"name";s:13:"MSSA 8.7.2017";s:4:"type";s:8:"fieldset";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:0:"";}i:1;a:7:{s:2:"id";s:2:"29";s:4:"slug";s:13:"kipparin-nimi";s:4:"name";s:13:"Kipparin nimi";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:11:"Jani Rajala";}i:2;a:7:{s:2:"id";s:2:"30";s:4:"slug";s:16:"sahkopostiosoite";s:4:"name";s:18:"Sähköpostiosoite";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:23:"rajala.jani@saunahti.fi";}i:3;a:7:{s:2:"id";s:2:"31";s:4:"slug";s:38:"puhelinnumero-nyt-ja-tapahtuman-aikana";s:4:"name";s:38:"Puhelinnumero nyt ja tapahtuman aikana";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:10:"0400800196";}i:4;a:7:{s:2:"id";s:2:"32";s:4:"slug";s:10:"kotisatama";s:4:"name";s:10:"Kotisatama";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:11:"Vuolenkoski";}i:5;a:7:{s:2:"id";s:2:"33";s:4:"slug";s:9:"vene-kone";s:4:"name";s:11:"Vene + kone";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:22:"Yamarin 600 Yamaha 250";}i:6;a:7:{s:2:"id";s:2:"34";s:4:"slug";s:26:"matkavauhtisi-tapahtumassa";s:4:"name";s:26:"Matkavauhtisi tapahtumassa";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:9:"55 solmua";}i:7;a:7:{s:2:"id";s:2:"35";s:4:"slug";s:20:"venekunnan-koko-hloa";s:4:"name";s:24:"Venekunnan koko (hlöä)";s:4:"type";s:4:"text";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:1:"2";}i:8;a:7:{s:2:"id";s:2:"37";s:4:"slug";s:21:"valitse-veneen-luokka";s:4:"name";s:21:"Valitse veneen luokka";s:4:"type";s:6:"select";s:7:"options";s:129:"a:4:{i:0;s:15:"historic-luokka";i:1;s:21:"luokka 1 (alle 35 kn)";i:2;s:19:"luokka 2 (35-50 kn)";i:3;s:20:"luokka 3 (yli 50 kn)";}";s:9:"parent_id";s:1:"0";s:5:"value";s:15:"historic-luokka";}i:9;a:7:{s:2:"id";s:2:"26";s:4:"slug";s:9:"varmennus";s:4:"name";s:9:"Varmennus";s:4:"type";s:12:"verification";s:7:"options";s:0:"";s:9:"parent_id";s:1:"0";s:5:"value";s:0:"";}i:10;a:7:{s:2:"id";s:2:"27";s:4:"slug";s:28:"syota-kaksi-numeroa-kenttaan";s:4:"name";s:32:"Syötä kaksi numeroa kenttään";s:4:"type";s:6:"secret";s:7:"options";s:0:"";s:9:"parent_id";s:2:"26";s:5:"value";s:2:"02";}i:11;a:7:{s:2:"id";s:2:"28";s:4:"slug";s:6:"laheta";s:4:"name";s:8:"Lähetä";s:4:"type";s:6:"submit";s:7:"options";s:0:"";s:9:"parent_id";s:2:"26";s:5:"value";s:0:"";}}';

// Unserialized array
$unserialized = unserialize($serialized);

// Changing class name
$unserialized[8]['value'] = "luokka 3 (yli 50 kn)";

// Structure of the changed array
// var_dump($unserialized);

// Array, serialized with change
$serialized_back = serialize($unserialized);

// Echoing data, which can be copy-pasted to database
echo $serialized_back;